package neural;

import utils.MathUtils;

import java.util.ArrayList;

public class Neuron {

   // private static final int BIAS = 1;//смещение
    private static final double LEARNING_RATIO = 0.1;//скорость обучения

    private ArrayList<Integer> inputs;
    private ArrayList<Double> weights;
   // private double biasWeight;//смещение веса
    private double output;

    public Neuron() {
        this.inputs = new ArrayList<>();
        this.weights = new ArrayList<>();
        //this.biasWeight = Math.random();
    }

    public void setInputs(ArrayList<Integer> inputs) {
        if (this.inputs.size() == 0) {
            this.inputs = new ArrayList<>(inputs);
            generateWeights();
        }

        this.inputs = new ArrayList<>(inputs);
    }

    private void generateWeights() {
        for (int i = 0; i < inputs.size(); i++) {
            weights.add(Math.random());
        }
    }

    /**
     * расчет ошибки
     */
    public void calculateOutput() {//расчитываем выход от нейронов
        double sum = 0;     /*это наше net */

        for (int i = 0; i < inputs.size(); i++) {//до количества наших нейронов
            sum += inputs.get(i) * weights.get(i);//вес умножить на нейрон
        }
        //sum += BIAS * biasWeight;
        output = MathUtils.sigmoidValue(sum);//это выход нейрона
    }
//для расчета дельты
    public void adjustWeights(double delta) {
        for (int i = 0; i < inputs.size(); i++) {
            double d = weights.get(i);
            d += LEARNING_RATIO * delta * inputs.get(i);//закон корректировки весов
            //корректируем вес для каждого значения
            weights.set(i, d);
        }
       // biasWeight += LEARNING_RATIO * delta * BIAS;
    }

    public double getOutput() {
        calculateOutput();
        return output;
    }

}
