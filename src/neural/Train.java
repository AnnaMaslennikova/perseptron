package neural;

import data.ReadWriteFile;

import java.util.ArrayList;

public class Train {

    private static final int NEURON_COUNT = 26;//количество нейронов

    private Network network;//сеть
    private ArrayList<TrainingSet> trainingSets;//список наборов обучаемых

    public Train() {
        this.network = new Network();
        this.network.addNeurons(NEURON_COUNT);//добавляем 26 нейронов в нашу нейросеть
        this.trainingSets = ReadWriteFile.readTrainingSets();//наборы сичтываем из файла и создаем их
    }
//вытаскиваем идеальную(образцовую выборку из файлов)
    public void train(long count) {
        for (long i = 0; i < count; i++) {
            int index = ((int) (Math.random() * trainingSets.size()));
            TrainingSet set = trainingSets.get(index);
            network.setInputs(set.getInputs());//устанавливаем значение
            network.adjustWages(set.getGoodOutput());//находим дельту
        }
    }

    public void setInputs(ArrayList<Integer> inputs) {
        network.setInputs(inputs);
    }

    public void addTrainingSet(TrainingSet newSet) {
        trainingSets.add(newSet);
    }

    public ArrayList<Double> getOutputs() {
        return network.getOutputs();
    }

}
