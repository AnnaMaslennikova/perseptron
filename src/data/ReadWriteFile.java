package data;

import neural.TrainingSet;

import java.io.*;
import java.util.ArrayList;

public class ReadWriteFile {

    /**
     * Создание правильных выходных векторов для всех букв алфавита
     * @return 
     */
    public static ArrayList<TrainingSet> readTrainingSets() {
        ArrayList<TrainingSet> trainingSets = new ArrayList<>();
        try {
            for (int i = 0; i < 26; i++) {
                char letterValue = (char) (i + 65);
                String letter = String.valueOf(letterValue);
                for (ArrayList<Integer> list : readFromFile( letter + ".txt")) {
                    trainingSets.add(new TrainingSet(list, GoodOutputs.getInstance().getGoodOutput(letter)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return trainingSets;
    }

    /**
     * считка из файла обучающей выборки
     * @param filename
     * @return 
     */
    private static ArrayList<ArrayList<Integer>> readFromFile(String filename) {
        ArrayList<ArrayList<Integer>> inputs = new ArrayList<>();
        System.out.println("filename in read file: "+filename);
        try {
            InputStream in = ReadWriteFile.class.getClass().getResourceAsStream(filename);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                ArrayList<Integer> input = new ArrayList<>();
                for (int i = 0; i < line.length(); i++) {
                    int value = 0;
                    try {
                        value = Integer.parseInt(String.valueOf(line.charAt(i)));
                    } catch (Exception e) {
                    }
                    input.add(value);
                }
                inputs.add(input);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return inputs;
    }

    /**
     * сохраняет в файл входной вектор 
     * @param input - входной вектор 
     * @param filename - имя файла
     */
    public static void saveToFile(ArrayList<Integer> input, String filename) {
        try {
            System.out.println("filename in save file: "+filename);
            File file = new File(/*"resources/" + */filename + ".txt");
            PrintWriter pw = new PrintWriter(new FileOutputStream(file, true));
            for (Integer i : input) {
                pw.write(Integer.toString(i));
            }
            pw.write("\n");
            pw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
